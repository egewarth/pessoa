#ifndef PROFESSOR_H
#define PROFESSOR_H

#include "pessoa.hpp"

class Professor : public Pessoa {
	private:
	       int matricula;
	       int carga_horaria;
               int salario;
               string projetos;
               string disciplinas;
	       
	public:
        	Professor();
                Professor(string nome, string idade, string telefone, int matricula, int carga_horaria, int salario, string projetos, string disciplinas);
                int getMatricula();
                void setMatricula(int matricula);
                int getCargaHoraria();
                void setCargaHoraria(int carga_horaria);
                int getSalario();
                void setSalario(int salario);
                string getProjetos();
                void setProjetos(string projetos);
                string getDisciplinas();
                void setDisciplinas(string disciplinas);
};



#endif