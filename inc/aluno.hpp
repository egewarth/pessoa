#ifndef ALUNO_H
#define ALUNO_H

#include "pessoa.hpp"

class Aluno : public Pessoa {
	private:
		int matricula;
		int quantidade_de_creditos;
		int semestre;
		float ira;
	public:
		Aluno();
        Aluno(string nome, string idade, string telefone, int matricula, int creditos, int semestre, float ira);
        int getMatricula();
        void setMatricula(int matricula);
        int getQuantidadeCreditos();
        void setQuantidadeCreditos(int creditos);
        int getSemestre();
        void setSemestre(int semestre);
        float getIra();
        void setMatricula(float ira);
};



#endif