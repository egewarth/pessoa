#include <iostream>
#include "pessoa.hpp"
#include "aluno.hpp"

using namespace std;

int main(int argc, char ** argv) {
	Pessoa aluno_1;
	Pessoa * aluno_2;
	Pessoa * aluno_3;
	
	aluno_1.setNome("Bruno");
	aluno_1.setTelefone("555-4444");
	aluno_1.setIdade("14");

	aluno_2 = new Pessoa(); 

	aluno_2->setNome("Maria");
	aluno_2->setTelefone("333-5555");
	aluno_2->setIdade("54");

	aluno_3 = new Pessoa("Joao","35","222-5555");

	cout << "Nome\tIdade\tTelefone" << endl;
	cout << aluno_1.getNome() << "\t" << aluno_1.getIdade() << "\t" << aluno_1.getTelefone() << endl;
	cout << aluno_2->getNome() << "\t" << aluno_2->getIdade() << "\t" << aluno_2->getTelefone() << endl;
	cout << aluno_3->getNome() << "\t" << aluno_3->getIdade() << "\t" << aluno_3->getTelefone() << endl;

	delete(aluno_2);
	delete(aluno_3);

}
