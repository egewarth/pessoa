#include <iostream>
#include "aluno.hpp"

Professor::Professor(){

	setNome("");
	setIdade("");
	setTelefone("");
	setMatricula("");
	setCargaHoraria("");
	setSalario("");
	setProjetos("");
	setDisciplinas("");
}
Professor::Professor(string nome, string idade, string telefone, int matricula, int carga_horaria, int salario, string projetos, string disciplinas){
	setNome(nome);
	setIdade(idade);
	setTelefone(telefone);
	setMatricula(matricula);
	setCargaHoraria(carga_horaria);
	setSalario(salario);
	setProjetos(projetos);
	setDisciplinas(disciplinas);
}

string  Professor::getNome() {
        return nome;
}

void Professor::setNome(string nome) {
        this->nome = nome;
}

string  Professor::getIdade() {
        return idade;
}

void Professor::setIdade(string idade) {
        this->idade = idade;
}

string  Professor::getTelefone() {
        return telefone;
}

void Professor::setTelefone(string telefone) {
        this->telefone = telefone;
}

int Professor::getMatricula() {
        return matricula;
}

void Professor::setMatricula(int matricula) {
        this->matricula = matricula;
}

int Professor::getCargaHoraria() {
        return carga_horaria;
}

void Professor::setCargaHoraria (int carga_horaria) {
        this->carga_horaria = carga_horaria;
}

int Professor::getSalario() {
        return salario;
}

void Professor::setSalario(int salario) {
        this-> salario = salario;
}

int Professor::getProjetos() {
        return projetos;
}
void Professor::setProjetos(string projetos) {
        this->projetos = projetos;
}
int Professor::getDisciplinas() {
        return disciplinas;
}
void Professor::setProjetos(string disciplinas) {
        this->disciplinas = disciplinas;
}
