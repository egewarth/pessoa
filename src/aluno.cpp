#include <iostream>
#include "aluno.hpp"

Aluno::Aluno(){

	setNome("");
	setIdade("");
	setTelefone("");
	setMatricula("");
	setQuantidadeCreditos("");
	setSemestre("");
	setIra("");
}
Aluno::Aluno(string nome, string idade, string telefone, int matricula, int quantidade_de_creditos, int semestre, float ira){
	setNome(nome);
	setIdade(idade);
	setTelefone(telefone);
	setMatricula(matricula);
	setQuantidadeCreditos(quantidade_de_creditos);
	setSemestre(semestre);
	setIra(ira);
}

string  Aluno::getNome() {
        return nome;
}

void Aluno::setNome(string nome) {
        this->nome = nome;
}

string  Aluno::getIdade() {
        return idade;
}

void Aluno::setIdade(string idade) {
        this->idade = idade;
}

string  Aluno::getTelefone() {
        return telefone;
}

void Aluno::setTelefone(string telefone) {
        this->telefone = telefone;
}

int Aluno::getMatricula() {
        return matricula;
}

void Aluno::setMatricula(int matricula) {
        this->matricula = matricula;
}

int Aluno::getQuantidadeCreditos() {
        return quantidade_de_creditos;
}

void Aluno::setQuantidadeCreditos(int creditos) {
        quantidade_de_creditos = creditos;
}

int Aluno::getSemestre() {
        return semestre;
}

void Aluno::setSemestre(int semestre) {
        this-> semestre = semestre;
}

int Aluno::getIra() {
        return ira;
}

void Aluno::setIra(int ira) {
        this->ira = ira;
}
